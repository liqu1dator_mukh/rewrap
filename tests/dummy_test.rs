#![allow(clippy::unit_cmp)]
#![allow(clippy::type_complexity)]

use rewrap::Rewrap;
use thiserror::Error;

#[test]
fn test() {
    #[derive(Error, Debug)]
    enum Error {}

    let result: Result<(), Box<dyn std::error::Error>> = Ok(());
    let nested_result: Result<Result<(), Box<dyn std::error::Error>>, Error> = Ok(result);
    // Here we converting Result<Result<(), Box<dyn std::error::Error>>, Error> into Result<(), Box<dyn Error>>
    let result_with_std_error = nested_result.rewrap();
    result_with_std_error.unwrap();
}

#[rustfmt::skip]
#[test]
fn very_complex_and_overkill_test() {
    let test: Result<Result<Result<Result<(), ()>, ()>, ()>, ()> = Ok(Ok(Ok(Ok(()))));
    assert_eq!(
        test.unwrap()
            .unwrap()
            .unwrap()
            .unwrap(),
        test.rewrap()
            .rewrap()
            .rewrap()
            .unwrap()
    );
}
