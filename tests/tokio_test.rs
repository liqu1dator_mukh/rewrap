use rewrap::Rewrap;
use tokio::task::JoinError;

#[tokio::test]
async fn not_real_test() {
    tokio::spawn(async { Ok::<(), JoinError>(()) })
        .await
        .rewrap()
        .unwrap();
}

#[tokio::test]
async fn real_test() {
    use thiserror::Error;

    #[derive(Error, Debug)]
    enum Error {
        #[error(transparent)]
        JoinError(#[from] tokio::task::JoinError),
    }

    #[allow(clippy::unused_async)]
    async fn async_fn() -> Result<(), Error> {
        Ok(())
    }

    tokio::spawn(async_fn()).await.rewrap().unwrap_or_else(|why| {
        eprintln!("Error: {why}");
    });
}
