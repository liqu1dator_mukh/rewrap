#![no_std]
#![deny(missing_docs)]
#![doc = include_str!("../README.md")]

/// See rewrap()
pub trait Rewrap<Value, TargetError> {
    /// Converts `Result<Result<Value, TargetError>, Error>` into `Result<Value, TargetError>`, where `Error` implements `Into<TargetError>`.
    /// See more examples in tests.
    /// ```
    ///# use std::any::{Any, type_name as std_type_name, TypeId};
    /// use rewrap::Rewrap;
    ///# use thiserror::Error;
    ///# #[derive(Error, Debug)]
    ///# enum OriginalError {}
    ///# #[derive(Error, Debug)]
    ///# enum TargetError {
    ///# #[error(transparent)]
    ///# Original(#[from] OriginalError)
    ///# }
    /// let result: Result<(), TargetError> = Ok(());
    /// let result_of_result: Result<Result<(), TargetError>, OriginalError> = Ok(result);
    /// let rewrapped_result: Result<(), TargetError> = result_of_result.rewrap();
    /// let unwrapped_result: () = rewrapped_result.unwrap();
    /// ```
    #[allow(clippy::missing_errors_doc)]
    fn rewrap(self) -> Result<Value, TargetError>;
}

impl<Value, Error, TargetError> Rewrap<Value, TargetError>
    for Result<Result<Value, TargetError>, Error>
where
    Error: Into<TargetError>,
{
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn rewrap(self) -> Result<Value, TargetError> {
        self.unwrap_or_else(|why| Err(why.into()))
    }
}
