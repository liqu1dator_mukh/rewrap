# Rewrap

Ever wanted to convert `Result<Result<T, E>, F>` into `Result<T, F>`? No? Anyway...

## Installation

```bash
cargo add rewrap
```

## Usage

```rust
use rewrap::Rewrap;
use thiserror::Error;

#[derive(Error, Debug)]
enum OriginalError {}

#[derive(Error, Debug)]
enum TargetError {
    #[error(transparent)]
    Original(#[from] OriginalError)
}

fn main() {
    let result: Result<(), TargetError> = Ok(());
    let result_of_result: Result<Result<(), TargetError>, OriginalError> = Ok(result);
    let rewrapped_result: Result<(), TargetError> = result_of_result.rewrap();
    let unwrapped_result: () = rewrapped_result.unwrap();
}
```
